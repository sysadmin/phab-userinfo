use anyhow::Result;
use reqwest::{Client, ClientBuilder};
use serde::{Deserialize, Serialize};
use std::convert::From;
use structopt::StructOpt;
use warp::reply::{self, Json};
use warp::Filter;

#[derive(Debug, Deserialize, Serialize)]
struct PhabUserInfo {
    phid: String,
    #[serde(rename = "userName")]
    user_name: String,
    #[serde(rename = "realName")]
    real_name: String,
    #[serde(rename = "primaryEmail")]
    primary_email: String,
}

#[derive(Debug, Deserialize, Serialize)]
struct PhabResult {
    result: PhabUserInfo,
}

#[derive(Debug, Deserialize, Serialize)]
struct UserInfo<'a> {
    sub: &'a str,
    username: &'a str,
    name: &'a str,
    email: &'a str,
}

impl warp::reject::Reject for ReqwestErrorWrapper {}
#[derive(Debug)]
struct ReqwestErrorWrapper(reqwest::Error);

impl From<reqwest::Error> for ReqwestErrorWrapper {
    fn from(e: reqwest::Error) -> Self {
        ReqwestErrorWrapper(e)
    }
}

async fn handle_do(client: Client, url: String, auth: String) -> Result<Json, ReqwestErrorWrapper> {
    let phab_reply: PhabResult = client
        .get(&url)
        .header("Authorization", auth)
        .send()
        .await?
        .json()
        .await?;

    let info = UserInfo {
        sub: &phab_reply.result.phid,
        username: &phab_reply.result.user_name,
        name: &phab_reply.result.real_name,
        email: &phab_reply.result.primary_email,
    };

    Ok(reply::json(&info))
}

async fn handle(
    client: Client,
    url: String,
    auth: String,
) -> Result<Json, warp::reject::Rejection> {
    handle_do(client, url, auth)
        .await
        .map_err(warp::reject::custom)
}

#[derive(Clone, Debug, StructOpt)]
struct Opt {
    #[structopt(
        env = "URL",
        default_value = "https://phabricator.collabora.com/api/user.whoami"
    )]
    url: String,
    #[structopt(env = "PORT", default_value = "8080")]
    port: u16,
}

#[tokio::main]
async fn main() -> Result<()> {
    env_logger::init();
    // Phabricator doesn't parse headers case-insensitively ;/
    let client = ClientBuilder::new().http1_title_case_headers().build()?;

    let opt = Opt::from_args();
    let url = opt.url;
    let userinfo = warp::path!("userinfo")
        .and(warp::header("Authorization"))
        .and_then(move |auth| handle(client.clone(), url.clone(), auth));

    warp::serve(userinfo).run(([0, 0, 0, 0], opt.port)).await;
    Ok(())
}
