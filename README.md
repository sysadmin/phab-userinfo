# Phab userinfo

Small helper to translate the userinfo endpoint provided by phabricator into a
more standard json format as support by e.g. the grafana generic oauth
authentication helper

```
phab-userinfo 0.1.0

USAGE:
    phab-userinfo [ARGS]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

ARGS:
    <url>      [env: URL=]  [default: https://phabricator.collabora.com/api/user.whoami]
    <port>     [env: PORT=]  [default: 8080]
```

The using service should get its oauth api/userinfo URL configured to this
service on the `/userinfo` path; Calls need to be made using bearer
authentication which is valid for phabricator (This is the default for oauth
services it seems).

The service doesn't terminate SSL connection and *must* be use either only
a private network (e.g. in a docker-compose) or behind a ssl terminating proxy.
