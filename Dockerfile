FROM debian:buster-slim
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
  apt-get install -y --no-install-recommends libssl1.1 ca-certificates

COPY target/release/phab-userinfo /usr/local/bin

EXPOSE 8080/tcp
ENTRYPOINT [ "/usr/local/bin/phab-userinfo" ]

